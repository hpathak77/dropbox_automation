package com.dropbox.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogoutPage extends BaseUi {

	public LogoutPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// locator for logo image in logout page
	@FindBy(className = "dropbox-logo__type")
	public WebElement dropboxLogoImage;
}
