package com.dropbox.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.dropbox.util.Utilities;

public class FilesPage extends BaseUi {

	public FilesPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// locator for Home Page Header
	@FindBy(xpath = "//h1[contains(@class,'page-header-text')]")
	public WebElement filePageTitle;

	// locator for New folder text box
	@FindBy(xpath = "//input[contains(@class,'browse-file-row__input')]") 
	public WebElement newFolderNameTextBox;

	// locator for create New folder button
	@FindBy(xpath = "//button[contains(@class,'action-new-folder')]")
	public WebElement createNewFolderButton;

	// locator for email text box in share file model
	@FindBy(id = "unified-share-modal-contacts-tokenizer")
	public WebElement shareFileEmailTextBox;

	// locator for send button in share file model
	@FindBy(xpath = "//button[contains(@class,'unified-share-modal__share-send')]")
	public WebElement shareFileSendButton;

	// locator for toast message on file sharing
	@FindBy(id = "notify-msg")
	public WebElement fileSharedNotifyMessage;

	// list of member Ids to which the file is shared
	@FindBy(xpath = "//div[contains(@class,'sharing-member-info__details')]")
	public List<WebElement> sharedMemberIdList;

	// list of member names to which the file is shared
	@FindBy(xpath = "//div[contains(@class,'sharing-member-info__display-name')]")
	public List<WebElement> sharedMemberNameList;

	// Locator for delete file link in more action pop over
	@FindBy(xpath = "//div[contains(@class,'mc-popover-content')]/button[contains(@class,'action-delete')]")
	public WebElement deleteFileLink;

	// Locator for header of delete files confirmation pop up
	@FindBy(xpath = "//h2[@id='db-modal-title']/div[text()='Delete file?']")
	public WebElement deleteFilesTitle;

	// Locator for Delete button on confirmation pop up
	@FindBy(xpath = "//div[@class='db-modal-buttons']/button[text()='Delete']")
	public WebElement deleteFilesConfirmationPopUpDeleteButtonTitle;

	// Locator for notification on deleting a file
	@FindBy(xpath = "//span[text()='Deleted 1 item.']")
	public WebElement fileDeletedNotifyMessage;

	// Dynamic locator for uploaded file
	public WebElement uploadedFile(String fileName) {
		return driver.findElement(By.xpath("//a[contains(@class,'filename-link')]/span[text()='" + fileName + "']"));
	}

	// Dynamic locator for share button against a file name
	public WebElement shareButton(String fileName) {
		return driver.findElement(
				By.xpath("//li[@data-filename='" + fileName + "']/div/button[contains(@class,'inline-share-button')]"));
	}

	// Dynamic locator for share file model window title
	public WebElement shareModelTitle(String fileName) {
		return driver.findElement(By.xpath("//h3[text()='" + fileName + "']"));
	}

	// Dynamic locator for members column against a file name
	public WebElement membersLink(String fileName) {
		return driver.findElement(By
				.xpath("//li[@data-filename='" + fileName + "']/div/div[contains(@class,'shared-with-members')]/div"));
	}

	// Dynamic locator for more actions column against a file name
	public WebElement moreActionsLink(String fileName) {
		return driver.findElement(By.xpath("//li[@data-filename='" + fileName
				+ "']//div[@class='browse-overflow-menu']/div/button[contains(@class,'browse-overflow-menu-trigger')]"));
	}

	// return a flag to check uploaded file is displayed in file section
	public boolean verifyFileisUploaded(String fileName) {
		return isElemetPresent(By.xpath("//a[contains(@class,'filename-link')]/span[text()='" + fileName + "']"));
	}

	// Function to create a new folder
	public FilesPage createNewFolder(String folderName) {
		// click on create new folder button
		performClick(createNewFolderButton, "createNewFolderButton");
		// enter the folder name
		performSendKey(wait.until(ExpectedConditions.visibilityOf(newFolderNameTextBox)), "newFolderNameTextBox",
				folderName);
		// click on the page title to move focus
		performClick(filePageTitle, "filePageTitle");
		Utilities.hardWait(2);
		return PageFactory.initElements(driver, FilesPage.class);
	}

	// return a flag to check a folder name is displayed in file section
	public boolean verifyNewFolderIsCreated(String folderName) {
		return isElemetPresent(By.xpath("//li[contains(@data-filename,'" + folderName + "')]"));
	}

	// Function to share a file
	public FilesPage shareFile(String fileName, String memberId) {
		// Hover mouse on file name
		performMouseHoverJScript(uploadedFile(fileName), "fileName");
		// click on the share button
		performClickJScript(shareButton(fileName), "shareButton");
		wait.until(ExpectedConditions.visibilityOf(shareModelTitle(fileName)));
		// enter the member Id to which the file is being shared
		performSendKey(shareFileEmailTextBox, "shareFileEmailTextBox", memberId);
		// click on send button
		performClick(shareFileSendButton, "shareFileSendButton");
		wait.until(ExpectedConditions.visibilityOf(fileSharedNotifyMessage));
		Utilities.hardWait(2);
		return PageFactory.initElements(driver, FilesPage.class);
	}

	// Function to delete a file
	public FilesPage deleteFile(String fileName) {
		// click on more actions column against a file name
		performClick(moreActionsLink(fileName), "moreActions");
		// click on delete button in pop over
		performClick(deleteFileLink, "deleteFileLink");
		wait.until(ExpectedConditions.visibilityOf(deleteFilesTitle));
		// click on delete button on confirmation pop up
		performClick(deleteFilesConfirmationPopUpDeleteButtonTitle, "deleteFilesConfirmationPopUpDeleteButtonTitle");
		wait.until(ExpectedConditions.visibilityOf(fileDeletedNotifyMessage));
		Utilities.hardWait(1);
		return PageFactory.initElements(driver, FilesPage.class);
	}

}
