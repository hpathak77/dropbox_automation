package com.dropbox.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BaseUi {

	public LoginPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// locator for signIn button in application landing page
	@FindBy(id = "sign-in")
	public WebElement signInButton;

	// locator for emailId text box in login page
	@FindBy(xpath = "(//input[@name='login_email'])[1]")
	public WebElement emailTextBox;

	// locator for password text box in login page
	@FindBy(xpath = "(//input[@name='login_password'])[1]")
	public WebElement passwordTextBox;

	// locator for signIn button in login page
	@FindBy(xpath = "(//div[@class='sign-in-text']/parent :: button[contains(@class,'login-button')])[1]")
	public WebElement loginButton;

	// Function for Login
	public HomePage login(String userName, String password) {
		// Enter email Id
		performSendKey(emailTextBox, "emailTextBox", userName);
		// enter password
		performSendKey(passwordTextBox, "passwordTextBox", password);
		// click on login button
		performClick(loginButton, "loginButton");
		return PageFactory.initElements(driver, HomePage.class);
	}

}
