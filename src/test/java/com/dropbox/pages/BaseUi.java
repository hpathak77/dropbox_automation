package com.dropbox.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

public class BaseUi {

	public static WebDriver driver;
	public static WebDriverWait wait;
	public static JavascriptExecutor js;

	public BaseUi(WebDriver driver) {
		BaseUi.driver = driver;
		wait = new WebDriverWait(driver, 30);
		js = (JavascriptExecutor) driver;
		PageFactory.initElements(driver, this);
	}
	
	//function to verify page titile
	public boolean verifyPageTitle(String pageTitle) {
		String webpagetitle = driver.getCurrentUrl();
		return webpagetitle.contains(pageTitle);
	}

	//refresh the page
	public void pageRefresh() {
		driver.navigate().refresh();
	}

	//override click for logging the event
	public void performClick(WebElement element, String elementName) {
		wait.until(ExpectedConditions.elementToBeClickable(element)).click();
		Reporter.log(elementName + " is clicked");
	}

	//override clear for logging the event
	public void performClear(WebElement element, String elementName) {
		wait.until(ExpectedConditions.visibilityOf(element)).clear();
		Reporter.log("value of " + elementName + " is cleared");
	}

	//override sendkeys for logging the event
	public void performSendKey(WebElement element, String elementName, String elementValue) {
		wait.until(ExpectedConditions.visibilityOf(element)).sendKeys(elementValue);
		Reporter.log(elementValue + " value entered for " + elementName);
	}

	//override select for logging the event
	public void performSelect(WebElement element, String elementName, String elementValue) {
		wait.until(ExpectedConditions.visibilityOf(element));
		Select select1 = new Select(element);
		select1.selectByVisibleText(elementValue);
		Reporter.log(elementValue + " value selected for " + elementName);
	}

	//override mouse hover for logging the event
	public void performMouseHover(WebElement element, String elementName) {
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		Reporter.log("Mouse is hovered over " + elementName);
	}

	//mouse hover using java script
	public void performMouseHoverJScript(WebElement element, String elementName) {
		String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		js.executeScript(mouseOverScript, element);
		Reporter.log("Mouse is hovered over " + elementName);
	}

	//click using java script executer
	public void performClickJScript(WebElement element, String elementName) {
		js.executeScript("arguments[0].click();", element);
		Reporter.log(elementName + " is clicked");
	}

	//function to verify if any element is present or not
	public boolean isElemetPresent(WebElement element) {
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			return element.isDisplayed();
		} catch (NoSuchElementException e) {
			System.out.println("Catch " + element);
			return false;
		}
	}

	//function to verify if any locator is present or not
	public boolean isElemetPresent(By by) {
		try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(by)));
			return driver.findElement(by).isDisplayed();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Catch " + by);
			return false;
		}
	}
}
