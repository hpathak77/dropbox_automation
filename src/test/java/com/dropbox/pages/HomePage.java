package com.dropbox.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.dropbox.util.Utilities;

public class HomePage extends BaseUi {

	public HomePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// locator for Home Page Header
	@FindBy(className = "page-header__heading")
	public WebElement homePageTitle;

	// locator for My account button
	@FindBy(xpath = "//button[@aria-label='Account menu']")
	public WebElement accountmenuButton;

	// locator for Sign out link
	@FindBy(linkText = "Sign out")
	public WebElement signOutButton;

	// locator for Upload files button
	@FindBy(xpath = "//button[contains(@class,'primary-action-menu__button')]")
	public WebElement uploadFilesButton;

	// locator for Upload to dropbox title
	@FindBy(xpath = "//h2[@id='db-modal-title']/div[text()='Upload to Dropbox']")
	public WebElement uploadToDropboxTitle;

	// locator for Choose files button
	@FindBy(xpath = "//button[text()='Choose files']")
	public WebElement chooseFilesButton;

	// locator for checked image on attaching a file
	@FindBy(xpath = "//img[contains(@class,'s_web_s_check')]")
	public WebElement attachFileCompletionConfirmationImage;

	// locator for Done button
	@FindBy(xpath = "//button[text()='Done']")
	public WebElement doneButton;

	// locator for files link in Home Page
	@FindBy(id = "files")
	public WebElement filesLink;

	// function to select and upload a file in user's dropbox account
	public HomePage uploadFile(String filePath, String fileName) {
		// click on upload files button
		performClick(uploadFilesButton, "uploadFilesButton");
		// Used hard wait as choose button is responding a bit late
		Utilities.hardWait(5);
		// click on choose files button
		performClick(chooseFilesButton, "chooseFilesButton");
		// Used hard wait as webdriver wait can not wait for a windows' model
		Utilities.hardWait(5);
		// Attach a file usin java robo
		attachFileWithGivenName(filePath, fileName);
		wait.until(ExpectedConditions.visibilityOf(attachFileCompletionConfirmationImage));
		// click on done button
		performClick(doneButton, "doneButton");
		return PageFactory.initElements(driver, HomePage.class);
	}

	// function to navigate to files page
	public FilesPage navigateToFilesPage() {
		performClick(filesLink, "filesLink");
		return PageFactory.initElements(driver, FilesPage.class);
	}

	// Function to search a file in given folder and attach a file with the given name
	private void attachFileWithGivenName(String filePath, String fileName) {
		File folder = new File(filePath);
		File[] listOfFiles = folder.listFiles();
		if (listOfFiles.length > 0) {
			for (File file : listOfFiles) {
				if (file.getName().contains(fileName)) {
					enterFilePathInWindow(file.getAbsolutePath());
					break;
				}
			}
		} else {
			Assert.assertTrue(false, "No file is available to upload");
		}
	}

	/*private void attachFirstFileFromAFolder(String filePath) {
		File folder = new File(filePath);
		File[] listOfFiles = folder.listFiles();
		if (listOfFiles.length > 0) {
			enterFilePathInWindow(listOfFiles[0].getAbsolutePath());
		} else {
			Assert.assertTrue(false, "No file is available to upload");
		}
	}*/

	// function to handle the file upload using Java Robot
	private void enterFilePathInWindow(String fileAbsolutePath) {
		// get the absolute path for the file
		StringSelection s = new StringSelection(fileAbsolutePath);
		// copy the file path in clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(s, null);
		Robot robot;
		try {
			// paste the absolute path in to browse window and hit enter
			robot = new Robot();
			robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
			robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
			robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
			robot.keyPress(java.awt.event.KeyEvent.VK_V);
			robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
			Utilities.hardWait(3);
			robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
			Reporter.log("File path is entered ");
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// function to perform logout
	public LogoutPage logOut() {
		// click on account menu button
		performClick(accountmenuButton, "accountmenuButton");
		// click on sign out button
		performClick(signOutButton, "signOutButton");
		return PageFactory.initElements(driver, LogoutPage.class);
	}
}
