package com.dropbox.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Utilities {
	
	public static void hardWait(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {}
	}
	
	public static String getFirstFileNameInFolder(String folderPath){
		File folder = new File(folderPath);
		File[] listOfFiles = folder.listFiles();
		return listOfFiles[0].isFile() ? listOfFiles[0].getName() : "";
	}
	
	// To generate five digit random number
	public static String randomNumberFiveDigit() {
		Random r = new Random(System.currentTimeMillis());
		int i = 10000 + r.nextInt(20000);
		return Integer.toString(i);
	}
	
	public static void createNewTextFile(String folderPath, String fileName){
		BufferedWriter output = null;
		try {
			File file = new File(folderPath + "/" + fileName + ".txt");
			output = new BufferedWriter(new FileWriter(file));
			output.write(fileName);
			output.close();
		} catch (IOException io) {
			io.printStackTrace();
		}
	}

}
