package com.dropbox.tests;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.dropbox.pages.FilesPage;
import com.dropbox.pages.HomePage;
import com.dropbox.pages.LoginPage;
import com.dropbox.util.EnviormentSetting;
import com.dropbox.util.Utilities;

public class FileTests extends BaseTest {

	@Test(testName = "FileUpload", description = "Verify User is able to upload a file in applicationa and uploaded file should be displayed in My Files section")
	public void verifyFileUploadFunctionality() {
		// Generate a random file name
		String fileName = "dropboxUpload_" + Utilities.randomNumberFiveDigit();
		// Create a text file
		Utilities.createNewTextFile(filePath, fileName);
		// Launch the url
		LoginPage loginPage = new LoginPage(driver);
		// Click on Sign In button to perform login
		loginPage.performClick(loginPage.signInButton, "signInButton");
		// Login to the application using valid userId and Password
		HomePage homePage = loginPage.login(EnviormentSetting.settings.get("UserName"),
				EnviormentSetting.settings.get("Password"));
		// Upload the text file and navigate to Files Page
		FilesPage filesPage = homePage.uploadFile(filePath, fileName).navigateToFilesPage();
		// Verify user is able to upload a file and file should display in Files section
		Assert.assertTrue(filesPage.isElemetPresent(filesPage.uploadedFile(fileName + ".txt")),
				"User should be able to upload file and new file should dispayed in Files section");
		Reporter.log("File is uploaded");
	}

	@Test(testName = "FileShare", description = "Verify User is able to share his file with other member")
	public void verifyFileShareFunctionality() throws AWTException {
		// Generate a random file name
		String fileName = "dropboxUpload_" + Utilities.randomNumberFiveDigit();
		String memberId = EnviormentSetting.settings.get("MemberId");
		// Create a text file
		Utilities.createNewTextFile(filePath, fileName);
		// Launch the url
		LoginPage loginPage = new LoginPage(driver);
		// Click on Sign In button to perform login
		loginPage.performClick(loginPage.signInButton, "signInButton");
		// Login to the application using valid userId and Password
		HomePage homePage = loginPage.login(EnviormentSetting.settings.get("UserName"),
				EnviormentSetting.settings.get("Password"));
		// Upload the text file and navigate to Files Page
		FilesPage filesPage = homePage.uploadFile(filePath, fileName).navigateToFilesPage();
		// Verify user is able to upload a file and file should display in Files section
		Assert.assertTrue(filesPage.isElemetPresent(filesPage.uploadedFile(fileName + ".txt")),
				"User should be able to upload file and new file should dispayed in Files section");
		Reporter.log("File is uploaded");
		// Share the file with a member
		filesPage.shareFile(fileName + ".txt", memberId);
		// click on the members column against the shared file
		filesPage.performClick(filesPage.membersLink(fileName + ".txt"), fileName);
		// Verify Owner Id is displayed in Members list
		Assert.assertTrue(
				filesPage.sharedMemberIdList.get(0).getText().equals(EnviormentSetting.settings.get("UserName")),
				"Owner Id should be displayed in member list");
		Reporter.log(EnviormentSetting.settings.get("UserName") + "is displayed in member list");
		// Verify Member Id is displayed in Members list
		Assert.assertTrue(
				(filesPage.sharedMemberIdList.get(1).getText().equals(memberId)
						|| filesPage.sharedMemberNameList.get(1).getText().equals(memberId)),
				"Member Id should be displayed in member list");
		Reporter.log(memberId + "is displayed in member list");
	}

	@Test(testName = "FileDelete", description = "Verify User is able to delete a file")
	public void verifyFileDeleteFunctionality() throws AWTException {
		// Generate a random file name
		String fileName = "dropboxUpload_" + Utilities.randomNumberFiveDigit();
		// Create a text file
		Utilities.createNewTextFile(filePath, fileName);
		// Launch the url
		LoginPage loginPage = new LoginPage(driver);
		// Click on Sign In button to perform login
		loginPage.performClick(loginPage.signInButton, "signInButton");
		// Login to the application using valid userId and Password
		HomePage homePage = loginPage.login(EnviormentSetting.settings.get("UserName"),
				EnviormentSetting.settings.get("Password"));
		// Upload the text file and navigate to Files Page
		FilesPage filesPage = homePage.uploadFile(filePath, fileName).navigateToFilesPage();
		// Verify user is able to upload a file and file should display in Files section
		Assert.assertTrue(filesPage.isElemetPresent(filesPage.uploadedFile(fileName + ".txt")),
				"User should be able to upload file and new file should dispayed in Files section");
		Reporter.log("File is uploaded");
		// Delete the file
		filesPage.deleteFile(fileName + ".txt");
		// Verify user should be able to delete the file and deleted file should
		// not display in Files section
		Assert.assertFalse(filesPage.verifyFileisUploaded(fileName + ".txt"),
				"User should be able to delete file and deleted file should not dispay in Files section");
		Reporter.log("File is Deleted");
	}

}
