package com.dropbox.tests;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.dropbox.pages.FilesPage;
import com.dropbox.pages.HomePage;
import com.dropbox.pages.LoginPage;
import com.dropbox.util.EnviormentSetting;
import com.dropbox.util.Utilities;

public class NewFolderTests extends BaseTest {

	@Test
	public void verifyCreateNewFolderFunctionality(){
		// Generate a random folder name
		String folderName = "Dropbox Automation_" + String.valueOf(Utilities.randomNumberFiveDigit());
		// Launch the url
		LoginPage loginPage = new LoginPage(driver);
		// Click on Sign In button to perform login
		loginPage.performClick(loginPage.signInButton, "signInButton");
		// Login to the application using valid userId and Password
		HomePage homePage = loginPage.login(EnviormentSetting.settings.get("UserName"),
				EnviormentSetting.settings.get("Password"));
		//click on files link and navigate to Files section
		homePage.performClick(homePage.filesLink, "filesLink");
		FilesPage filesPage = new FilesPage(driver);
		//Create a new folder with randomly generated name
		filesPage.createNewFolder(folderName);
		//Verify new folder should be created and displayed in Files Section
		Assert.assertTrue(filesPage.verifyNewFolderIsCreated(folderName),
				"User should be able to create a new folder and new folder should displayed in My files section");
		Reporter.log(folderName + " folder is created.");
	}

}
