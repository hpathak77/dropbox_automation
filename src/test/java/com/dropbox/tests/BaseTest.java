package com.dropbox.tests;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.apache.commons.io.FileUtils;

import com.dropbox.util.EnviormentSetting;

public class BaseTest {
	protected static WebDriver driver;
	protected static WebDriverWait wait;
	protected static HashMap<String, String> setting;
	protected static String filePath;

	public static WebDriver getDriver() {
		return driver;
	}
	
	@BeforeSuite
	public void cleanTempData(){
		setupTestdata();
		try {
			//Clean the temp files created as test data
			FileUtils.cleanDirectory(new File(filePath));
			//Clean the screenshots for previous run 
			//FileUtils.cleanDirectory(new File("/target/screenshots"));
		} catch (IOException | IllegalArgumentException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}
	
	@Parameters({ "browser", "appUrl" })
	@BeforeMethod
	public void beforeMethod(String browser, String appUrl, ITestResult result) {
		setupBrowser(browser, appUrl, result);
		setupWait();
		//setupTestdata();
	}
	
	@AfterMethod
	public void afterMethod(ITestResult result) {
		Reporter.setCurrentTestResult(result);
		if (null != driver) {
			driver.quit();
			Reporter.log("Driver is closed");
		}
	}

	private void setupBrowser(String browser, String appUrl, ITestResult result) {
		Reporter.setCurrentTestResult(result);
		switch (browser) {
		case "iExplorer":
			System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			Reporter.log("ieExplorer browser is launched");
			break;
		case "firefox":
			System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
			driver = new FirefoxDriver();
			Reporter.log("Firefox browser is launched");
			break;
		case "chrome":
			ChromeOptions options = new ChromeOptions();
			options.addArguments("chrome.switches", "--disable-extensions");
			options.addArguments("test-type");
			System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
			driver = new ChromeDriver(options);
			Reporter.log("Chrome browser is launched");
			break;
		default:
			System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
			driver = new FirefoxDriver();
			Reporter.log("Firefox browser is launched");
			break;
		}
		driver.get(appUrl);
		Reporter.log(appUrl + " is launched");
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
	}

	private void setupWait() {
		wait = new WebDriverWait(driver, 15);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(240, TimeUnit.SECONDS);
	}
	
	private void setupTestdata(){
		new EnviormentSetting("src/test/resources/testdata/Settings.xls", "Settings");
		setting = EnviormentSetting.settings;
		filePath = "src/test/resources/uploadFile";
	}
}
