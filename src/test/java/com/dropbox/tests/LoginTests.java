package com.dropbox.tests;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.dropbox.pages.HomePage;
import com.dropbox.pages.LoginPage;
import com.dropbox.pages.LogoutPage;
import com.dropbox.util.EnviormentSetting;

public class LoginTests extends BaseTest {

	@Test(testName = "LoginLogout", description = "Verify User is able to login successfully with valid credential and from home page user has option to log out from the applicaion")
	public void verifyLoginLogOut() {
		//Launch the url
		LoginPage loginPage = new LoginPage(driver);
		//Click on Sign In button to perform login
		loginPage.performClick(loginPage.signInButton, "signInButton");
		//Login to the application using valid userId and Password
		HomePage homePage = loginPage.login(EnviormentSetting.settings.get("UserName"),
				EnviormentSetting.settings.get("Password"));
		//Verify user is able to login successfully and landed to Home Page
		Assert.assertTrue(homePage.isElemetPresent(homePage.homePageTitle),
				"User should be able to navigate to Home Page after login with valid username and password");
		Reporter.log("User has logged in and landed on Home Page");
		//Perform log out
		LogoutPage logoutPage = homePage.logOut();
		//Verify user can log out from the application successfully
		Assert.assertTrue(homePage.isElemetPresent(logoutPage.dropboxLogoImage) & driver.getCurrentUrl().contains("logout"),
				"User should be able to logout successfully");
		Reporter.log("User has logged logged out and landed on Logout Page");
	}

}
